<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <title>GNU Social Map</title>
    <link type="text/css" rel="stylesheet" href="css/style.css"/>
  </head>
  <body>
    <img src="images/logo.png" id="logo">
  	<a href="#modal" class="button">WHAT IS THIS?</a>

	<div id="waiting">Loading... Please Wait...</div>

  	<div id="map"></div>
  	
  	<div id="modal">
  		<a href="#" class="button">CLOSE</a>

  		<div id="explain">
  			<div class="text">
	  			<p>
	  				<?php require_once('config.php'); ?>
	  				This is a visualization of the <a href="https://gnu.io/social/">GNU Social</a> (and friends) network, obtained crawling the relationships follower/followed across the fediverse. On the main screen you seen the direct relationship between top accounts (those having more than <?php echo $users_threeshold ?> followers), here a more concise rappresentation of the most connected instances (those with more than <?php echo $instances_threeshold ?> external connections).
	  			</p>
	  			<p>
	  				It is eventually updated sometime, at least until my server's IP is not banned for API abuse.
	  			</p>

	  			<hr/>

	  			<p>
	  				Forged by <a href="http://madbob.org/">Roberto -MadBob- Guido</a>, <a href="https://gitlab.com/madbob/socialmap">grab here the code</a>.
	  			</p>
  			</div>
  		</div>

  		<div id="chord"></div>
  	</div>

    <script type="text/javascript" src="js/d3.js"></script>
    <script type="text/javascript" src="js/sm.js"></script>
  </body>
</html>

