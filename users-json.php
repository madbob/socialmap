<?php

require_once('utils.php');

$db = connect_db();

$data = (object)[
	'nodes' => [],
	'links' => []
];

$query = "SELECT accounts.id AS id, accounts.name AS name, COUNT(connections.follower) AS c
			FROM accounts, connections
			WHERE accounts.id = connections.followed
			GROUP BY connections.followed
			HAVING c > $users_threeshold";
$user_results = $db->query($query);
$map = [];
$index = 0;

while($row = $user_results->fetch_object()) {
	$map[$row->id] = $index++;

	$data->nodes[] = (object) [
		'name' => $row->name,
		'id' => $row->id,
		'rating' => 0
	];
}

$query = "SELECT * FROM connections";
$connections_results = $db->query($query);

while($row = $connections_results->fetch_object()) {
	if(!isset($map[$row->follower]) || !isset($map[$row->followed]))
		continue;

	$data->links[] = (object) [
		'source' => $map[$row->follower],
		'target' => $map[$row->followed]
	];
	
	$data->nodes[$map[$row->followed]]->rating++;
}

header('Content-Type: application/json');
echo json_encode($data);

