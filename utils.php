<?php

require_once('config.php');

function connect_db() {
	global $hostname, $username, $password, $dbname;
	return new mysqli($hostname, $username, $password, $dbname);
}

