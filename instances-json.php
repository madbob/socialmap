<?php

require_once('utils.php');

function fix_domain($domain) {
	$domain = str_replace('.', '_', $domain);
	$domain = str_replace('https://', '', $domain);
	$domain = str_replace('http://', '', $domain);
	return $domain;
}

$db = connect_db();

$query = "SELECT instances.domain AS destination, instances2.domain AS source, COUNT(connections.followed) AS stars FROM
			instances, instances AS instances2, accounts, accounts AS accounts2, connections WHERE
			instances.id = accounts.instance_id AND
			accounts.id = connections.followed AND
			connections.follower = accounts2.id AND
			accounts2.instance_id = instances2.id
			GROUP BY instances2.domain, instances.id HAVING stars > $instances_threeshold";

$results = $db->query($query);

$current = '';
$data = [];

while($row = $results->fetch_object()) {
	$source_name = fix_domain($row->source);
	if (!isset($data[$source_name])) {
		$data[$source_name] = (object) [
			'name' => $source_name,
			'imports' => []
		];
	}

	$destination_name = fix_domain($row->destination);
	if (!isset($data[$destination_name])) {
		$data[$destination_name] = (object) [
			'name' => $destination_name,
			'imports' => []
		];
	}
	
	$data[$source_name]->imports[] = $destination_name;
}

$data = array_values($data);
header('Content-Type: application/json');
echo json_encode($data);

