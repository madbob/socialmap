<?php

require_once('utils.php');

class Crawler {
	private $db;
	private $instances_cache = [];
	
	function do_get($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$reponse = curl_getinfo($ch);
		curl_close($ch);

		usleep(300000);

		if ($reponse['http_code'] != 200)
			return $reponse['http_code'];
		else
			return json_decode($result);
	}

	function handle_user($url, $expected_name) {
		$test = parse_url($url);
		$domain = sprintf('%s://%s', $test['scheme'], $test['host']);
		$instance_id = $this->retrieve_instance($domain);

		preg_match('/^.*\/([^\/]*)$/', $url, $matches, PREG_OFFSET_CAPTURE);
		$local_id = $matches[1][0];
		$query = sprintf("SELECT id FROM accounts WHERE instance_id = %d AND local_id = '%s'", $instance_id, $local_id);
		$test = $this->db->query($query);
		if ($test->num_rows == 0) {
			$query = sprintf("INSERT INTO accounts (instance_id, local_id, name, to_update) VALUES (%d, '%s', '%s', true)", $instance_id, $local_id, $expected_name);
			$this->db->query($query);
			$account_id = $this->db->insert_id;
			echo 'N';
		}
		else {
			$r = $test->fetch_object();
			$account_id = $r->id;
			echo '.';
		}
	
		return [$account_id, $domain, $local_id];
	}

	function read_followers($owner_id) {
		$query = sprintf("SELECT instance_id, local_id, to_update, unavailable FROM accounts WHERE id = %d", $owner_id);
		$test = $this->db->query($query);
		if ($test->num_rows == 0)
			return;

		$r = $test->fetch_object();
		if ($r->to_update == false)
			return;

		$local_id = $r->local_id;
		$instance_id = $r->instance_id;

		$query = sprintf("UPDATE accounts SET to_update = false WHERE instance_id = %d AND local_id = '%s'", $instance_id, $local_id);
		$this->db->query($query);
		
		if ($r->unavailable == true)
			return;

		$domain = $this->retrieve_instance_reverse($instance_id);

		$url = sprintf('%s/api/statuses/followers.json?id=%s', $domain, $local_id);
		$data = $this->do_get($url);
		if(is_numeric($data)) {
			if ($data == 404) {
				$query = sprintf("UPDATE accounts SET unavailable = true WHERE instance_id = %d AND local_id = '%s'", $instance_id, $local_id);
				$this->db->query($query);
			}
			else {
				echo "\nError code $data accessing $local_id on $domain\n";
			}
			
			return;
		}
	
		$connections = [];
		$query = sprintf("SELECT follower FROM connections WHERE follower = %d", $owner_id);
		$test = $this->db->query($query);
		while($r = $test->fetch_object()) {
			$connections[$r->follower] = true;
		}
	
		$real_connections = [];

		foreach($data as $d) {
			if (isset($d->ostatus_uri)) {
				$reference_url = $d->ostatus_uri;
			}
			else if (isset($d->statusnet_profile_url)) {
				$reference_url = $d->statusnet_profile_url;
			}
			else {
				print_r($d);
				continue;
			}

			list($connection_id, $other_domain, $other_local_id) = $this->handle_user($reference_url, $d->name);
			if (!isset($connections[$connection_id])) {
				$query = sprintf("INSERT INTO connections (follower, followed) VALUES (%d, %d)", $connection_id, $owner_id);
				$this->db->query($query);
			}

			$real_connections[] = $connection_id;
		}

		$query = sprintf("DELETE FROM connections WHERE followed = %d AND follower NOT IN (%s)", $owner_id, join(', ', $real_connections));
		$this->db->query($query);
	}

	function retrieve_instance($domain) {
		if (isset($this->instances_cache[$domain]))
			return $this->instances_cache[$domain];

		$query = sprintf("SELECT id FROM instances WHERE domain = '%s'", $domain);
		$test = $this->db->query($query);
		if ($test->num_rows == 0) {
			$query = sprintf("INSERT INTO instances (domain) VALUES ('%s')", $domain);
			$this->db->query($query);
			$instance_id = $this->db->insert_id;
		}
		else {
			$r = $test->fetch_object();
			$instance_id = $r->id;
		}
	
		$this->instances_cache[$domain] = $instance_id;
		return $instance_id;
	}
	
	function retrieve_instance_reverse($instance_id) {
		foreach($this->instances_cache as $domain => $id)
			if ($id == $instance_id)
				return $domain;

		$query = sprintf("SELECT domain FROM instances WHERE id = %d", $instance_id);
		$test = $this->db->query($query);
		if ($test->num_rows == 0) {
			return null;
		}
		else {
			$r = $test->fetch_object();
			$domain = $r->domain;
		}
	
		$this->instances_cache[$domain] = $instance_id;
		return $domain;
	}

	function crawl($starting_point) {
		$instance_id = $this->retrieve_instance($starting_point);

		$startup_url = sprintf('%s/api/statuses/public_timeline.as', $starting_point);
		$initial_set = $this->do_get($startup_url);
		if (is_numeric($initial_set))
			return;

		foreach($initial_set->items as $item) {
			$this->handle_user($item->actor->id, $item->actor->displayName);
		}
	
		while(true) {
			$query = sprintf("SELECT id, local_id FROM accounts WHERE to_update = true AND unavailable = false LIMIT 50");
			$test = $this->db->query($query);
			if($test->num_rows == 0)
				break;

			while($r = $test->fetch_object()) {
				$this->read_followers($r->id);
			}
		}
	}
	
	public function __construct() {
		$this->db = connect_db();
	}

	public function do_the_job() {
		$this->db->query("UPDATE accounts SET to_update = true WHERE unavailable = false");
		$this->crawl('https://quitter.se');
	}
}

$crawler = new Crawler();
$crawler->do_the_job();

